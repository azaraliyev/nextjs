import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="https://cbmes.com">CBMES</a>
        </h1>

        <p className={styles.description}>
          Always ready!
        </p>

        <Link href="/users">
          <a>Users</a>
        </Link>

        <div className={styles.grid}>
          <a href="https://rms.cbmes.com" className={styles.card}>
            <h3>RMS &rarr;</h3>
            <p>Reports Management System.</p>
          </a>

          <a href="https://act.cbmes.com" className={styles.card}>
            <h3>ACT &rarr;</h3>
            <p>Action Tracking.</p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://cbmes.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="https://cbmes.com/images/logo.png" alt="CBMES Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
