import Head from 'next/head'
import Link from 'next/link'
import { Menu, Dropdown } from 'antd'
import { DownOutlined, UserOutlined } from '@ant-design/icons'

import styles from '../styles/Home.module.css'

const menu = (items) => (
  <Menu>
    {items.data.map((user) => (
      <Menu.Item>
        <Link href={`/users/${user.id}`}>{user.first_name}</Link>
      </Menu.Item>
    ))}
  </Menu>
);

export default function Users({ users }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Dropdown overlay={menu(users)}>
          <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
            <UserOutlined /> Users <DownOutlined />
          </a>
        </Dropdown>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://cbmes.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="https://cbmes.com/images/logo.png" alt="CBMES Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}

export async function getStaticProps(context) {
  const res = await fetch(`https://reqres.in/api/users`)
  const users = await res.json()

  if (!users) {
    return {
      notFound: true
    }
  }

  return {
    props: {
      users,
    },

    revalidate: 1
  }
}