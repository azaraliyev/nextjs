import Head from 'next/head'
import Link from 'next/link'
import { Button } from 'antd'
import styles from '../../styles/Home.module.css'

export default function User({user}) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Link href="/users">
          <Button>Users</Button>
        </Link>
        <p className={styles.description}>
          {JSON.stringify(user)}
        </p>
      </main>
    </div>
  )
}

export async function getStaticPaths() {
  const res = await fetch('https://reqres.in/api/users')
  const users = await res.json()

  const paths = users.data.map((user) => ({
    params: { id: user.id.toString() }
  }))

  return { paths, fallback: true }
}

export async function getStaticProps({ params }) {
  const res = await fetch(`https://reqres.in/api/users/${params.id}`)
  const user = await res.json()

  return {
    props: { user },
    revalidate: 1
  }
}